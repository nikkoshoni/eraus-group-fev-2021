import 'dart:async';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:eraus_group_app/UI/LoginOrSignup/ChoseLoginOrSignup.dart';
import 'package:eraus_group_app/UI/OnBoarding.dart';

import 'Library/Language_Library/lib/easy_localization_delegate.dart';
import 'Library/Language_Library/lib/easy_localization_provider.dart';

/// Run first apps open
void main() => runApp(EasyLocalization(child: MyApp()));

/// Set orienttation
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    /// To set orientation always portrait
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    ///Set color status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return EasyLocalizationProvider(
      data: data,
      child: new MaterialApp(
        title: "Eraüs Group",
        theme: ThemeData(
            brightness: Brightness.light,
            backgroundColor: Colors.white,
            primaryColorLight: Colors.white,
            primaryColorBrightness: Brightness.light,
            primaryColor: Colors.white),
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
        localizationsDelegates: [
          EasylocaLizationDelegate(
            locale: data.locale,
            path: 'assets/language',
          )
        ],
        supportedLocales: [
          Locale('en','US'),Locale('zh','HK'),Locale('ar','DZ'),Locale('hi','IN'),Locale('id','ID')
        ],
        locale: data.savedLocale,

      ),
    );
  }
}

/// Component UI
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

/// Component UI
class _SplashScreenState extends State<SplashScreen> {

  /// Check user
  bool _checkUser = true;


  SharedPreferences prefs;

  Future<Null> _function() async {
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    this.setState(() {
      if (prefs.getString("username") != null) {
        print('false');
        _checkUser = false;
      } else {
        print('true');
        _checkUser = true;
      }
    });
  }


  @override
  /// Setting duration in splash screen
  startTime() async {
    return new Timer(Duration(milliseconds: 3000), navigatorPage);
  }
  /// To navigate layout change
  void navigatorPage() {
    if(_checkUser){
      /// if userhas never been login
      Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> OnBoarding()));
    }else{
      /// if userhas ever been login
      Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> ChoseLogin()));
    }
  }
  /// Declare startTime to InitState
  @override
  void initState() {
    super.initState();
    startTime();
    _function();
  }
  /// Code Create UI Splash Screen
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          /// Set Background image in splash screen layout (Click to open code)
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/imgIllustration/eraus_group_logo.jpg'), fit: BoxFit.contain)),
        ),
      ),
    );
  }
}
