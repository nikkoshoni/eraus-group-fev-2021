class Marchandise {
  final String id;
  final String img;
  final List<String> imgs;
  final String marque;
  final String modele;
  final String price;
  final String chassis;
  final String dateArriveePort;
  final String anneeMiseCirculation;
  final String paysOrigine;
  final String description;

  Marchandise(
      {this.id,
      this.img,
      this.marque,
      this.modele,
      this.price,
      this.chassis,
      this.dateArriveePort,
      this.description,
      this.imgs,
      this.anneeMiseCirculation,
      this.paysOrigine});
}

List<Marchandise> gridItemArray = [
  Marchandise(
      id: "1",
      img: "assets/img/porsche-3370038_1920.jpg",
      marque: "Porsche",
      modele: "Modèle",
      price: "\$ 20",
      dateArriveePort: "31/12/2020",
      chassis: "BJJKJS4JZ",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "2",
      img: "assets/img/auto-788747_1920.jpg",
      marque: "Mustang",
      modele: "Modèle",
      price: "\$ 20",
      dateArriveePort: "31/12/2020",
      chassis: "BJNJBBSJ",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "3",
      img: "assets/img/auto-2179220_1920.jpg",
      marque: "BENZ",
      modele: "Modèle",
      price: "\$ 20",
      dateArriveePort: "31/12/200",
      chassis: "BHHBHZ",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "4",
      img: "assets/img/bmw-768688_1920.jpg",
      marque: "BMW Sport",
      modele: "Modèle",
      price: "\$ 20",
      dateArriveePort: "31/03/2021",
      chassis: "HJZ788982",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "5",
      img: "assets/img/bmw-918408_1920.jpg",
      marque: "BMW New",
      modele: "Modèle",
      price: "\$ 20",
      dateArriveePort: "12/02/2021",
      chassis: "NNJEB8983",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "6",
      img: "assets/img/car-604019_1920.jpg",
      marque: "BMW",
      modele: "Modèle",
      price: "\$ 3",
      dateArriveePort: "30/01/2021",
      chassis: "HHZ8788Z",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "7",
      img: "assets/img/car-1957037_1920.jpg",
      marque: "Rosie",
      price: "\$ 15",
      modele: "Modèle",
      dateArriveePort: "04/03/2021",
      chassis: "BHZI9889",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
  Marchandise(
      id: "8",
      img: "assets/img/ford-63930_1920.jpg",
      marque: "Ferrari",
      price: "\$ 100",
      modele: "Modèle",
      dateArriveePort: "03/03/2021",
      chassis: "JHJHD898989",
      description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
      anneeMiseCirculation: "2009",
      paysOrigine: 'Angleterre'),
];
