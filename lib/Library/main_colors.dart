import 'package:flutter/material.dart';
class MainColor {
  static Color primary = Color.fromRGBO(16, 64, 103, 1);
  static Color secondary = Color.fromRGBO(135, 14, 21, 1);
  static Color third = Color.fromRGBO(18, 79, 137, 1);
  static Color fourth = Color.fromRGBO(79, 79, 79, 1);
}