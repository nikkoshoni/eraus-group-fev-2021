class Brand {
  final String id;
  final String name;
  final String img;
  final String desc;
  final items item;

  const Brand({
    this.img,
    this.id,
    this.name,
    this.desc,
    this.item,
  });
}

class items {
  final String itemName;
  final String itemPrice;
  final String itemRatting;
  final String itemSale;
  final String itemId;
  final String itemImg;

  const items(
      {this.itemName,
        this.itemPrice,
        this.itemRatting,
        this.itemSale,
        this.itemId,
        this.itemImg});
}

List<Brand> brandData = [
  const Brand(
      name: "Transitaires",
      id: "1",
      img: "assets/img/man.png",
      desc:
      'Les transitaires s\'occupent du dédouanement pour vos achats de marchandises',
      item: items(
          itemImg: "assets/img/womanface.jpg",
          itemId: "1",
          itemName: "KOULA Koko",
          itemPrice: "\$ 1500",
          itemRatting: "4.5",
          itemSale: "Cabinet Trans-Togo")),
  const Brand(
      name: "Transporteurs",
      id: "2",
      img: "assets/img/man.png",
      desc:
      "Les transporteurs se chargent du transport de vos marchandises jusqu’à destination,",
      item: items(
          itemImg: "assets/img/womanface.jpg",
          itemId: "1",
          itemName: "John Toto",
          itemPrice: "\$ 1500",
          itemRatting: "4.5",
          itemSale: "250 Sale")),
  const Brand(
      name: "Importateurs Locaux",
      id: "3",
      img: "assets/img/man.png",
      desc:
      "Les importateurs locaux vous proposent des produits locaux",
      item: items(
          itemImg: "assets/img/womanface.jpg",
          itemId: "1",
          itemName: "BOUKO Kilo",
          itemPrice: "\$ 250",
          itemRatting: "4.5",
          itemSale: "Transport Safe")),
  const Brand(
      name: "Importateurs étrangers",
      id: "4",
      img: "assets/img/man.png",
      desc:
      "Les importateurs proposent des marchandises",
      item: items(
          itemImg: "assets/img/womanface.jpg",
          itemId: "1",
          itemName: "Bossa Joko",
          itemPrice: "\$ 100",
          itemRatting: "4.5",
          itemSale: "Angleterre")),
  const Brand(
      name: "Producteurs",
      id: "5",
      img: "assets/img/man.png",
      desc:
      "Producteurs et fabricants de marchandises",
      item: items(
          itemImg: "assets/img/womanface.jpg",
          itemId: "1",
          itemName: "AZ cosmetics",
          itemPrice: "\$ 100",
          itemRatting: "Producteurs de crême de beauté",
          itemSale: "Angleterre")),
];
