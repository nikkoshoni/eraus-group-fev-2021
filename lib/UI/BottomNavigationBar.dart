import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/MarchandiseUi.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/Search.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/UI/BrandUIComponent/BrandLayout.dart';
import 'package:eraus_group_app/UI/CartUIComponent/CartLayout.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/Home.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/Profile.dart';

class BottomNavigationBarCustomized extends StatefulWidget {
  @override
  _BottomNavigationBarCustomizedState createState() =>
      _BottomNavigationBarCustomizedState();
}

class _BottomNavigationBarCustomizedState
    extends State<BottomNavigationBarCustomized> {
  int currentIndex = 0;

  /// Set a type current number a layout class
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return new Menu();
      case 1:
        return new SearchAppbar();
      case 2:
        return new brand();
      case 3:
        return new Cart();
        break;
      case 4:
        return new MarchandiseUi();
        break;
      case 5:
        return new Profil();
        break;
      default:
        return Menu();
    }
  }

  /// Build BottomNavigationBar Widget
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        body: callPage(currentIndex),
        bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
                canvasColor: Colors.white,
                textTheme: Theme.of(context).textTheme.copyWith(
                    caption:
                        TextStyle(color: Colors.black26.withOpacity(0.15)))),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: currentIndex,
              fixedColor: MainColor.secondary,
              onTap: (value) {
                currentIndex = value;
                setState(() {});
              },
              items: [
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home,
                      size: 23.0,
                    ),
                    title: Text(
                      'Acceuil',
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.search,
                      size: 24.0,
                    ),
                    title: Text(
                      AppLocalizations.of(context).tr('Rechercher'),
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
                BottomNavigationBarItem(
                    icon: Icon(Icons.group),
                    title: Text(
                      'Acteurs',
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
                BottomNavigationBarItem(
                    icon: Icon(Icons.shopping_cart),
                    title: Text(
                      'Commande',
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.format_color_fill,
                      size: 24.0,
                    ),
                    title: Text(
                      'Marchandises',
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person,
                      size: 24.0,
                    ),
                    title: Text(
                      'Profil',
                      style:
                          TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                    )),
              ],
            )),
      ),
    );
  }
}
