import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/ListItem/BrandDataList.dart';

class Privatemessage extends StatefulWidget {
  /// Declare Brand class and Get Data Name for Appbar (BrandDataList.dart)
  var brand;

  Privatemessage(this.brand);

  @override
  _PrivatemessageState createState() => _PrivatemessageState(brand);
}

class _PrivatemessageState extends State<Privatemessage>
    with TickerProviderStateMixin {
  final List<Msg> _messages = <Msg>[];
  final TextEditingController _textController = new TextEditingController();
  bool _isWriting = false;

  /// Declare Brand class
  var brand;

  _PrivatemessageState(this.brand);

  @override
  Widget build(BuildContext ctx) {
    /// Declare default User Name
    String defaultUserName = AppLocalizations.of(context).tr('name');
    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.4,
          title: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  height: 50.0,
                  width: 50.0,
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    image: DecorationImage(image: AssetImage(brand.itemImg)),
                  )),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  brand.itemName,
                  style: TextStyle(
                      fontFamily: "Gotik",
                      fontSize: 18.0,
                      color: Colors.black54),
                ),
              ),
            ],
          ),
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
          backgroundColor: Colors.white,
        ),
        body: Column(children: <Widget>[
          Flexible(
            child: _messages.length > 0
                ? Container(
                    child: ListView.builder(
                      itemBuilder: (_, int index) => _messages[index],
                      itemCount: _messages.length,
                      reverse: true,
                      padding: EdgeInsets.all(10.0),
                    ),
                  )
                : NoMessage(),
          ),
          Divider(height: 1.5),
          Container(
            child: _buildComposer(),
            decoration: new BoxDecoration(
                color: Theme.of(ctx).cardColor,
                boxShadow: [BoxShadow(blurRadius: 1.0, color: Colors.black12)]),
          ),
        ]),
      ),
    );
  }

  /// Component for typing text
  Widget _buildComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 9.0),
          child: Row(
            children: <Widget>[
              Container(
                child: IconButton(
                  icon: Icon(Icons.add),
                  onPressed: null,
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: new TextField(
                    controller: _textController,
                    onChanged: (String txt) {
                      setState(() {
                        _isWriting = txt.length > 0;
                      });
                    },
                    onSubmitted: _submitMsg,
                    decoration: new InputDecoration.collapsed(
                        hintText: 'Votre message',
                        hintStyle: TextStyle(
                            fontFamily: "Sans",
                            fontSize: 18.0,
                            color: Colors.black26)),
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 3.0),
                  child: Theme.of(context).platform == TargetPlatform.iOS
                      ? CupertinoButton(
                          child:
                              Text(AppLocalizations.of(context).tr('submit')),
                          onPressed: _isWriting
                              ? () => _submitMsg(_textController.text)
                              : null)
                      : IconButton(
                          icon: Icon(Icons.send),
                          onPressed: _isWriting
                              ? () => _submitMsg(_textController.text)
                              : null,
                        )),
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
                  border: new Border(top: new BorderSide(color: Colors.brown)))
              : null),
    );
  }

  void _submitMsg(String txt) {
    _textController.clear();
    setState(() {
      _isWriting = false;
    });
    Msg msg = new Msg(
      txt: txt,
      animationController: new AnimationController(
          vsync: this, duration: new Duration(milliseconds: 800)),
    );
    setState(() {
      _messages.insert(0, msg);
    });
    msg.animationController.forward();
  }

  @override
  void dispose() {
    for (Msg msg in _messages) {
      msg.animationController.dispose();
    }
    super.dispose();
  }
}

class Msg extends StatelessWidget {
  Msg({this.txt, this.animationController});

  final String txt;
  final AnimationController animationController;

  @override
  Widget build(BuildContext ctx) {
    String defaultUserName = AppLocalizations.of(ctx).tr('name');
    return new SizeTransition(
      sizeFactor: new CurvedAnimation(
          parent: animationController, curve: Curves.fastOutSlowIn),
      axisAlignment: 0.0,
      child: new Container(
        margin: const EdgeInsets.symmetric(vertical: 8.0),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.only(right: 18.0),
              child: new CircleAvatar(
                  backgroundColor: Colors.indigoAccent,
                  child: new Text(defaultUserName[0])),
            ),
            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(defaultUserName,
                      style: TextStyle(
                          fontFamily: "Gotik", fontWeight: FontWeight.w900)),
                  new Container(
                    margin: const EdgeInsets.only(top: 6.0),
                    child: new Text(txt),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NoMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Opacity(
                opacity: 0.5,
                child: Image.asset(
                  "assets/imgIllustration/IlustrasiMessage.png",
                  height: 220.0,
                )),
            Text(
              AppLocalizations.of(context).tr('notHaveMessage'),
              style: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Colors.black12,
                  fontSize: 17.0,
                  fontFamily: "Popins"),
            )
          ],
        ),
      ),
    ));
  }
}
