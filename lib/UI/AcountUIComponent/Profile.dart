import 'dart:io';

import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/gestion.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/Home.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/AboutApps.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/CallCenter.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/Message.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/languageSetting.dart';
import 'package:eraus_group_app/UI/LoginOrSignup/ChoseLoginOrSignup.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/CreditCardSetting.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/MyOrders.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/Notification.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/SettingAcount.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/LoginOrSignup/Signup.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

class Profil extends StatefulWidget {
  @override
  _ProfilState createState() => _ProfilState();
}

/// Custom Font
var _txt = TextStyle(
  color: Colors.black,
  fontFamily: "Sans",
);

/// Get _txt and custom value of Variable for Name User
var _txtName = _txt.copyWith(fontWeight: FontWeight.w700, fontSize: 17.0);

/// Get _txt and custom value of Variable for Edit text
var _txtEdit = _txt.copyWith(color: Colors.black26, fontSize: 15.0);

/// Get _txt and custom value of Variable for Category Text
var _txtCategory = _txt.copyWith(
    fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

class _ProfilState extends State<Profil> {
  ImagePicker _picker = ImagePicker();

  /// Custom text header for bottomSheet
  final _fontCostumSheetBotomHeader = TextStyle(
      fontFamily: "Berlin",
      color: Colors.black54,
      fontWeight: FontWeight.w600,
      fontSize: 16.0);

  /// Custom text for bottomSheet
  final _fontCostumSheetBotom = TextStyle(
      fontFamily: "Sans",
      color: Colors.black45,
      fontWeight: FontWeight.w400,
      fontSize: 16.0);
  FileImage _imgPicked;

  @override
  Widget build(BuildContext context) {
    /// Declare MediaQueryData
    MediaQueryData mediaQueryData = MediaQuery.of(context);

    /// To Sett PhotoProfile,Name and Edit Profile
    var _profile = Padding(
      padding: EdgeInsets.only(
        top: 185.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 100.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2.5),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: _imgPicked ??
                                AssetImage("assets/img/womanface.jpg"),
                            fit: BoxFit.fill)),
                  ),
                  Positioned(
                    bottom: -5.0,
                    right: -5.0,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                      child: IconButton(
                        iconSize: 30.0,
                        color: MainColor.third,
                        icon: Icon(Icons.camera_alt),
                        onPressed: () {
                          _modalBottomSheetRefine();
                        },
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  AppLocalizations.of(context).tr('name'),
                  style: _txtName,
                ),
              ),
            ],
          ),
          Container(),
        ],
      ),
    );

    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data,
      child: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              /// Setting Header Banner
              Container(
                height: 240.0,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                            "assets/imgIllustration/eraus_group_logo.jpg"),
                        fit: BoxFit.cover)),
              ),

              /// Calling _profile variable
              _profile,
              Padding(
                padding: const EdgeInsets.only(top: 360.0),
                child: Column(
                  /// Setting Category List
                  children: <Widget>[
                    /// Call category class
                    Category(
                      txt: 'Notifications',
                      padding: 35.0,
                      image: "assets/icon/notification.png",
                      icon: Icon(Icons.notifications, color: MainColor.secondary,),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new NotificationCustomized()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Paiements',
                      padding: 35.0,
                      image: "assets/icon/creditAcount.png",
                      icon: FaIcon(
                        FontAwesomeIcons.handshake,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new CreditCardSetting()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Discussions',
                      padding: 26.0,
                      image: "assets/icon/chat.png",
                      icon: FaIcon(FontAwesomeIcons.comments, color: MainColor.secondary,),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new Chat()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Mes achats',
                      padding: 23.0,
                      image: "assets/icon/truck.png",
                      icon: FaIcon(
                        FontAwesomeIcons.truck,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new Order()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Paramètres du compte',
                      padding: 30.0,
                      image: "assets/icon/setting.png",
                      icon: Icon(
                        Icons.settings,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new SettingAcount()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Signaler un problème',
                      padding: 30.0,
                      image: "assets/icon/callcenter.png",
                      icon: Icon(
                        Icons.announcement,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new CallCenter()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Langues',
                      padding: 30.0,
                      image: "assets/icon/language.png",
                      icon: Icon(
                        Icons.language,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new LanguageSetting()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: 'Gestion',
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                            new Gestion()));
                      },
                      padding: 30.0,
                      image: "assets/icon/language.png",
                      icon: Icon(
                        Icons.local_mall,
                        color: MainColor.secondary,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      padding: 38.0,
                      txt: 'A propos',
                      image: "assets/icon/aboutapp.png",
                      icon: Icon(
                        Icons.info_outline,
                        color: MainColor.secondary,
                      ),
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new AboutApps()));
                      },
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 20.0)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<FileImage> getImage(ImageSource source) async {
    var file = await _picker.getImage(source: source);
    return await FileImage(File(file.path));
  }

  void _modalBottomSheetRefine() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return SingleChildScrollView(
            child: new Container(
                height: 80.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        getImage(ImageSource.camera).then((value) {
                          setState(() {
                            _imgPicked = value;
                          });
                          Navigator.of(context).pop();
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt,
                            color: MainColor.primary,
                          ),
                          Text('Caméra', style: _fontCostumSheetBotom),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        getImage(ImageSource.gallery).then((value) {
                          setState(() {
                            _imgPicked = value;
                          });
                          Navigator.of(context).pop();
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.image, color: MainColor.primary),
                          Text('Galerie', style: _fontCostumSheetBotom),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.delete, color: Colors.redAccent),
                          Text(
                            'Supprimer',
                            style: _fontCostumSheetBotom,
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          );
        });
  }
}

/// Component category class to set list
class Category extends StatelessWidget {
  @override
  String txt, image;
  var icon;
  GestureTapCallback tap;
  double padding;

  Category({this.txt, this.image, this.icon, this.tap, this.padding});

  Widget build(BuildContext context) {
    return InkWell(
      onTap: tap,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 30.0),
            child: Row(
              children: <Widget>[
                Padding(padding: EdgeInsets.only(right: padding), child: icon),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(
                    txt,
                    style: _txtCategory,
                  ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black12, size: 10.0,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
