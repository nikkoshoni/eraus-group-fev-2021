import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/gestion_achat.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/gestion_marchandise.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/GestionMarchandisePhoto.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

class Gestion extends StatefulWidget {
  Gestion({Key key}) : super(key: key);

  _GestionState createState() => _GestionState();
}

/// Custom Font
var _txt = TextStyle(
  color: Colors.black,
  fontFamily: "Sans",
);

var _txtCategory = _txt.copyWith(
    fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

class _GestionState extends State<Gestion> {
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Gestion',
            style: TextStyle(
                fontFamily: "Gotik",
                fontWeight: FontWeight.w600,
                fontSize: 18.5,
                letterSpacing: 1.2,
                color: Colors.black87),
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              GestionTile(
                title: 'Marchandises',
                onTap: () {
                  Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => new GestionMarchandise()));
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              GestionTile(
                title: 'Achats',
                onTap: () {
                  Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => new GestionAchat()));
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              GestionTile(
                title: 'Documents',
                onTap: () {},
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              GestionTile(
                title: 'Factures',
                onTap: () {},
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              GestionTile(
                title: 'Agent Eraüs',
                onTap: () {
                  Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, __, ___) => new GestionPhotoMarchandise()));
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 70.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GestionTile extends StatelessWidget {
  var onTap, title, sub, lead, trail;

  /// Custom Font
  static TextStyle _txt = TextStyle(
    color: Colors.black,
    fontFamily: "Sans",
  );

  var _txtCategory = _txt.copyWith(
      fontSize: 14.5, color: MainColor.primary, fontWeight: FontWeight.bold);

  GestionTile({this.onTap, this.lead, this.sub, this.title, this.trail});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: _txtCategory,
      ),
      subtitle: sub,
      leading: Icon(
        Icons.arrow_forward_ios,
        color: Colors.black12,
      ),
      onTap: onTap,
    );
  }
}
