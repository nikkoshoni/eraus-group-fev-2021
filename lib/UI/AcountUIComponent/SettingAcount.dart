import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingAcount extends StatefulWidget {
  @override
  _SettingAcountState createState() => _SettingAcountState();
}

class _SettingAcountState extends State<SettingAcount> {
  static var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 17.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  static var _txtCustomSub = TextStyle(
    color: MainColor.secondary,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  bool enableEditing = false;

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              child: RaisedButton(
                onPressed: () {
                  setState(() {
                    enableEditing = !enableEditing;
                  });
                },
                color: enableEditing? MainColor.primary : Colors.white,
                  child: Text(enableEditing? 'Enrégistrer': 'Modifier', style: enableEditing? TextStyle(color: Colors.white):_txtCustomHead,),
              )
            )
          ],
          title: Text(
            'Paramètres du compte',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
                color: MainColor.primary,
                fontFamily: "Gotik"),
          ),
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                setting(
                  head: 'Nom',
                  sub1: 'KOUNKE',
                  enableEditing: enableEditing
                ),
                setting(
                  head: 'Prénoms',
                  sub1: 'Kokou Nico',
                  enableEditing: enableEditing
                ),
                setting(head: "Fonction", sub1: "Client Acheteur", enableEditing: enableEditing),
                setting(head: "Téléphone 1", sub1: "97136294 ", enableEditing: enableEditing),
                setting(head: "Téléphone 2", sub1: "00000000", enableEditing: enableEditing),
                setting(head: "Email", sub1: "nikkoshoninkounke@gmail.com", enableEditing: enableEditing),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget setting({var head, var sub1, var sub2, var sub3, var enableEditing}) {
    var _txtCustomHead = TextStyle(
      color: MainColor.primary,
      fontSize: 17.0,
      fontWeight: FontWeight.w600,
      fontFamily: "Gotik",
    );

     var _txtCustomSub = TextStyle(
      color: MainColor.secondary,
      fontSize: 15.0,
      fontWeight: FontWeight.w500,
      fontFamily: "Gotik",
    );
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: Text(
                  head,
                  style: _txtCustomHead,
                ),
              ),
              Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0,
                      color: MainColor.secondary
                    ),
                    initialValue: sub1,
                    readOnly: !enableEditing,
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 20.0),
                child: Divider(
                  color: Colors.black12,
                  height: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Setting extends StatelessWidget {
  static var _txtCustomHead = TextStyle(
    color: MainColor.primary,
    fontSize: 17.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  static var _txtCustomSub = TextStyle(
    color: MainColor.secondary,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );


  String head, sub1, sub2, sub3;

  Setting({this.head, this.sub1, this.sub2, this.sub3});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: Text(
                  head,
                  style: _txtCustomHead,
                ),
              ),
              Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: TextFormField(
                    textAlign: TextAlign.end,
                    style: _txtCustomSub,
                    initialValue: sub1,
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 20.0),
                child: Divider(
                  color: Colors.black12,
                  height: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
