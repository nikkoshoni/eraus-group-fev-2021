import 'dart:async';


import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/UI/BottomNavigationBar.dart';
import 'package:intl/intl.dart';
import 'package:some_calendar/some_calendar.dart';
import 'package:intl/date_symbol_data_local.dart';


class MarchandiseForm extends StatefulWidget {
  @override
  _MarchandiseFormState createState() => _MarchandiseFormState();
}


class _MarchandiseFormState extends State<MarchandiseForm> {

  /// Custom Text Header for Dialog after user succes payment
  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  /// Duration for popup card if user succes to payment
  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }


  /// Navigation to route after user succes payment
  void navigator() {
    Navigator.of(context).pop();
    Navigator.of(context).pop();
  }

  @override
  /// For radio button
  int tapvalue = 0;
  int tapvalue2 = 0;
  int tapvalue3 = 0;
  int tapvalue4 = 0;

  /// Custom Text
  var _customStyle = TextStyle(
      fontFamily: "Gotik",
      fontWeight: FontWeight.w800,
      color: Colors.black,
      fontSize: 17.0);

  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        /// Appbar
        appBar: AppBar(
          leading: InkWell(
              onTap: () {
                Navigator.of(context).pop(false);
              },
              child: Icon(Icons.arrow_back)),
          elevation: 0.0,
          title: Text(
            'Ajouter une marchandise',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
                color: Colors.black54,
                fontFamily: "Gotik"),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[

                  Setting(
                      head: 'Libellé',
                      sub1: 'BMW Sport 360',
                  ),
                  Setting(
                      head: 'Marque',
                      sub1: 'BMW',
                  ),
                  Setting(head: "Modèle", sub1: "Décapotable"),
                  Setting(head: "Châssis", sub1: "BJHJHSHJS "),
                  Setting(head: "Date d'arrivée au port", sub1: "31/07/2021"),
                  Setting(head: "Année de mise en circulation", sub1: "2009"),
                  Setting(head: "Description", sub1: "Très solide et nouvelle"),
                  /// Button pay
                  InkWell(
                    onTap: () {
                      _showDialog(context);
                      startTime();
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: MainColor.primary,
                          borderRadius: BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          'Ajouter',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 2.0),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }



  /// Card Popup if success payment
  _showDialog(BuildContext ctx) {
    showDialog(
      context: ctx,
      barrierDismissible: true,
      child: SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  'Succès',
                  style: _txtCustomHead,
                ),
              )),
          Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
                child: Text(
                  'La nouvelle marchandise a été ajoutée',
                  style: _txtCustomSub,
                ),
              )),
        ],
      ),
    );
  }

  Widget setting({var head, var sub1, var sub2, var sub3, var enableEditing}) {
    var _txtCustomHead = TextStyle(
      color: MainColor.primary,
      fontSize: 17.0,
      fontWeight: FontWeight.w600,
      fontFamily: "Gotik",
    );

    var _txtCustomSub = TextStyle(
      color: MainColor.secondary,
      fontSize: 15.0,
      fontWeight: FontWeight.w500,
      fontFamily: "Gotik",
    );
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: Text(
                  head,
                  style: _txtCustomHead,
                ),
              ),
              Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        color: MainColor.secondary
                    ),
                    initialValue: sub1,
                    readOnly: !enableEditing,
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 20.0),
                child: Divider(
                  color: Colors.black12,
                  height: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}



class Setting extends StatelessWidget {
  static var _txtCustomHead = TextStyle(
    color: MainColor.primary,
    fontSize: 17.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  static var _txtCustomSub = TextStyle(
    color: MainColor.secondary,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );


  String head, sub1, sub2, sub3;

  Setting({this.head, this.sub1, this.sub2, this.sub3});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: Text(
                  head,
                  style: _txtCustomHead,
                ),
              ),
              Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: TextFormField(
                    style: _txtCustomSub,
                    initialValue: sub1,
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 20.0),
                child: Divider(
                  color: Colors.black12,
                  height: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}