import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/gestion_achat.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/gestion_marchandise.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/PhotoProduit.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

class GestionPhotoMarchandise extends StatefulWidget {
  GestionPhotoMarchandise({Key key}) : super(key: key);

  _GestionPhotoMarchandiseState createState() =>
      _GestionPhotoMarchandiseState();
}

/// Custom Font
var _txt = TextStyle(
  color: Colors.black,
  fontFamily: "Sans",
);

var _txtCategory = _txt.copyWith(
    fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

class _GestionPhotoMarchandiseState extends State<GestionPhotoMarchandise> {
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Liste des marchandises sans images',
            style: TextStyle(
                fontFamily: "Gotik",
                fontWeight: FontWeight.w600,
                fontSize: 13,
                letterSpacing: 1,
                color: Colors.black87),
          ),
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              CustomTile(
                title: 'Benz',
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => PhotoProduit()));
                },
                sub: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('4 places'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('HGSHJKL'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('20/08/2020'),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              CustomTile(
                title: 'Benz',
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => PhotoProduit()));
                },
                sub: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('4 places'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('HGSHJKL'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('20/08/2020'),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              CustomTile(
                title: 'Benz',
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => PhotoProduit()));
                },
                sub: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('4 places'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('HGSHJKL'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('20/08/2020'),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
              CustomTile(
                title: 'Benz',
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => PhotoProduit()));
                },
                sub: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('4 places'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('HGSHJKL'),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                    ),
                    Text('20/08/2020'),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Divider(
                  color: Colors.black12,
                  height: 1.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class CustomTile extends StatelessWidget {
  var onTap, title, sub, lead, trail;

  /// Custom Font
  static TextStyle _txt = TextStyle(
    color: Colors.black,
    fontFamily: "Sans",
  );

  var _txtCategory = _txt.copyWith(
      fontSize: 14.5, color: MainColor.primary, fontWeight: FontWeight.bold);

  CustomTile({this.onTap, this.lead, this.sub, this.title, this.trail});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: _txtCategory,
      ),
      trailing: Text('3'),
      subtitle: sub,
      onTap: onTap,
    );
  }
}
