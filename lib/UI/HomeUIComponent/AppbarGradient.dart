import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/Notification.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/Search.dart';
import 'package:eraus_group_app/UI/AcountUIComponent/Message.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppbarGradient extends StatefulWidget {
  @override
  _AppbarGradientState createState() => _AppbarGradientState();
}

class _AppbarGradientState extends State<AppbarGradient> {
  String CountNotice = "4";

  /// Build Appbar in layout home
  @override
  Widget build(BuildContext context) {
    /// Create responsive height and padding
    final MediaQueryData media = MediaQuery.of(context);
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    var data = EasyLocalizationProvider.of(context).data;

    /// Create component in appbar
    return EasyLocalizationProvider(
      data: data,
      child: Container(
        padding: EdgeInsets.only(top: statusBarHeight),
        height: 80.0 + statusBarHeight,
        decoration: BoxDecoration(

            /// gradient in appbar
            gradient: LinearGradient(
                colors: [MainColor.secondary, MainColor.primary],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            /// if user click shape white in appbar navigate to search layout
            Container(
              margin: EdgeInsets.only(left: media.padding.left + 15),
              height: 37.0,
              width: MediaQuery.of(context).size.width / 2,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  shape: BoxShape.rectangle),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                    left: 17.0,
                  )),
                  Padding(
                    padding: EdgeInsets.only(top: 3.0),
                    child: ColorizeAnimatedTextKit(
                        repeatForever: true,
                        text: [
                          "ERAÜS GROUP",
                        ],
                        textStyle: TextStyle(
                            fontSize: 20.0,
                            fontFamily: "Horizon",
                            fontWeight: FontWeight.bold),
                        colors: [
                          MainColor.primary,
                          MainColor.secondary,
                          MainColor.third,
                          Colors.red,
                        ],
                        textAlign: TextAlign.start,
                        alignment: AlignmentDirectional
                            .topStart // or Alignment.topLeft
                        ),
                  ),
                ],
              ),
            ),

            InkWell(
              child: Stack(
                children: <Widget>[
                  IconButton(
                      icon: FaIcon(FontAwesomeIcons.commentDots),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new Chat()));
                      }),
                  CircleAvatar(
                    radius: 8.6,
                    backgroundColor: MainColor.secondary,
                    child: Text(
                      CountNotice,
                      style: TextStyle(fontSize: 13.0, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            /// Icon notification (if user click navigate to notification layout)
            InkWell(
              child: Stack(
                children: <Widget>[
                  IconButton(
                      icon: FaIcon(FontAwesomeIcons.bell),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new NotificationCustomized()));
                      }),
                  CircleAvatar(
                    radius: 8.6,
                    backgroundColor: MainColor.secondary,
                    child: Text(
                      CountNotice,
                      style: TextStyle(fontSize: 13.0, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
