import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/carousel_pro/carousel_pro.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/confirmation_commande.dart';
import 'package:eraus_group_app/models/Marchandise.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/UI/CartUIComponent/CartLayout.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/ChatItem.dart';
import 'package:eraus_group_app/UI/CartUIComponent/Delivery.dart';

import 'package:flutter_rating/flutter_rating.dart';
import 'package:eraus_group_app/UI/HomeUIComponent/ReviewLayout.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProducteurDetail extends StatefulWidget {
  Marchandise marchandise;

  ProducteurDetail(this.marchandise);

  @override
  _ProducteurDetailState createState() => _ProducteurDetailState(marchandise);
}

/// Detail Product for Recomended Grid in home screen
class _ProducteurDetailState extends State<ProducteurDetail> {
  double rating = 3.5;
  int starCount = 5;

  /// Declaration List item HomeGridItemRe....dart Class
  final Marchandise marchandise;

  _ProducteurDetailState(this.marchandise);

  @override
  static BuildContext ctx;
  int valueItemChart = 0;
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  /// BottomSheet for view more in specification
  void _bottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.black26,
              child: Padding(
                padding: const EdgeInsets.only(top: 2.0),
                child: Container(
                  height: 1500.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0))),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 20.0)),
                      Center(
                          child: Text(
                            AppLocalizations.of(context).tr('description'),
                            style: _subHeaderCustomStyle,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                        child: Text(
                            AppLocalizations.of(context).tr('longLorem'),
                            style: _detailText),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          AppLocalizations.of(context).tr('spesifications'),
                          style: TextStyle(
                              fontFamily: "Gotik",
                              fontWeight: FontWeight.w600,
                              fontSize: 15.0,
                              color: Colors.black,
                              letterSpacing: 0.3,
                              wordSpacing: 0.5),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                        child: Text(
                          AppLocalizations.of(context).tr('loremIpsum'),
                          style: _detailText,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  /// Custom Text black
  static var _customTextStyle = TextStyle(
    color: MainColor.secondary,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var _subHeaderCustomStyle = TextStyle(
      color: MainColor.primary,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Custom Text for Detail title
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  Widget build(BuildContext context) {
    List<Marchandise> marchandiseSimilaires = [
      Marchandise(
          id: "5",
          img: "assets/img/bmw-918408_1920.jpg",
          marque: "BMW New",
          modele: "Modèle",
          price: "\$ 20",
          dateArriveePort: "12/02/2021",
          chassis: "NNJEB8983",
          description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
          anneeMiseCirculation: "2009",
          paysOrigine: 'Angleterre'),
      Marchandise(
          id: "6",
          img: "assets/img/car-604019_1920.jpg",
          marque: "BMW",
          modele: "Modèle",
          price: "\$ 3",
          dateArriveePort: "30/01/2021",
          chassis: "HHZ8788Z",
          description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
          anneeMiseCirculation: "2009",
          paysOrigine: 'Angleterre'),
      Marchandise(
          id: "7",
          img: "assets/img/car-1957037_1920.jpg",
          marque: "Rosie",
          price: "\$ 15",
          modele: "Modèle",
          dateArriveePort: "04/03/2021",
          chassis: "BHZI9889",
          description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
          anneeMiseCirculation: "2009",
          paysOrigine: 'Angleterre'),
      Marchandise(
          id: "8",
          img: "assets/img/ford-63930_1920.jpg",
          marque: "Ferrari",
          price: "\$ 100",
          modele: "Modèle",
          dateArriveePort: "03/03/2021",
          chassis: "JHJHD898989",
          description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
          anneeMiseCirculation: "2009",
          paysOrigine: 'Angleterre'),
    ];
    List<Widget> marchandiseSimilairesItem = [];
    marchandiseSimilaires.forEach((marchandise) {
      marchandiseSimilairesItem.add(FavoriteItem(
        image: marchandise.img,
        title: marchandise.marque,
        Salary: marchandise.price,
        modele: marchandise.modele,
        anneMiseCirculation: marchandise.anneeMiseCirculation,
        paysOrigine: marchandise.paysOrigine,
      ));
      marchandiseSimilairesItem.add(
        Padding(padding: EdgeInsets.only(right: 10.0)),
      );
    });

    /// Variable Component UI use in bottom layout "Top Rated Products"
    var _suggestedItem = Padding(
      padding: const EdgeInsets.only(
          left: 15.0, right: 20.0, top: 30.0, bottom: 20.0),
      child: Container(
        height: 280.0,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Marchandises similaires', style: _subHeaderCustomStyle),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'Voir tout',
                    style: TextStyle(
                        color: MainColor.fourth,
                        fontFamily: "Gotik",
                        fontWeight: FontWeight.w700),
                  ),
                )
              ],
            ),
            Expanded(
              child: ListView(
                  padding: EdgeInsets.only(top: 20.0, bottom: 2.0),
                  scrollDirection: Axis.horizontal,
                  children: marchandiseSimilairesItem),
            ),
          ],
        ),
      ),
    );

    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        key: _key,
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  Navigator.of(context).push(PageRouteBuilder(
                      pageBuilder: (_, ___, ____) => new ChatItem()));
                },
                icon: FaIcon(
                  FontAwesomeIcons.commentDots,
                  color: MainColor.secondary,
                )),
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                    PageRouteBuilder(pageBuilder: (_, __, ___) => new ConfirmationCommande()));
              },
              child: Stack(
                alignment: AlignmentDirectional(-1.0, -0.8),
                children: <Widget>[
                  IconButton(
                      onPressed: null,
                      icon: Icon(
                        Icons.shopping_cart,
                        color: MainColor.secondary,
                      )),
                  /*CircleAvatar(
                    radius: 10.0,
                    backgroundColor: Colors.red,
                    child: Text(
                      valueItemChart.toString(),
                      style: TextStyle(color: Colors.white, fontSize: 13.0),
                    ),
                  ),*/
                ],
              ),
            ),
          ],
          elevation: 0.5,
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text(
            'Détails',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: MainColor.primary,
              fontSize: 17.0,
              fontFamily: "Gotik",
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    /// Header image slider
                    Container(
                      height: 300.0,
                      child: Hero(
                        tag: "hero-grid-${marchandise.id}",
                        child: Material(
                          child: new Carousel(
                            dotColor: MainColor.fourth,
                            dotIncreaseSize: 1.7,
                            dotBgColor: Colors.transparent,
                            autoplay: false,
                            boxFit: BoxFit.fill,
                            images: [
                              AssetImage(marchandise.img),
                              AssetImage(marchandise.img),
                              AssetImage(marchandise.img),
                            ],
                          ),
                        ),
                      ),
                    ),

                    /// Background white title,price and ratting
                    Container(
                      decoration:
                      BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                          color: Color(0xFF656565).withOpacity(0.15),
                          blurRadius: 1.0,
                          spreadRadius: 0.2,
                        )
                      ]),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, top: 10.0, right: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              marchandise.marque,
                              style: _customTextStyle,
                            ),
                            Padding(padding: EdgeInsets.only(top: 5.0)),
                            Text(
                              marchandise.price,
                              style: _customTextStyle,
                            ),
                            Padding(padding: EdgeInsets.only(top: 10.0)),
                            Divider(
                              color: Colors.black12,
                              height: 1.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0, bottom: 10.0),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 15.0),
                                    child: Text(
                                      'Châssis',
                                      style: TextStyle(
                                          color: MainColor.secondary,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.all(10.0),
                                      decoration: BoxDecoration(
                                        color: MainColor.fourth,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20.0)),
                                      ),
                                      child: Text(
                                        marchandise.chassis,
                                        style: TextStyle(color: Colors.white),
                                      )),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    /// Background white for chose Size and Color
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        height: 220.0,
                        width: 600.0,
                        decoration:
                        BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                            color: Color(0xFF656565).withOpacity(0.15),
                            blurRadius: 1.0,
                            spreadRadius: 0.2,
                          )
                        ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, left: 20.0, right: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Date d'arrivée au port",
                                  style: _subHeaderCustomStyle),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    marchandise.dateArriveePort,
                                    style: _customTextStyle,
                                  )
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 15.0)),
                              Divider(
                                color: Colors.black12,
                                height: 1.0,
                              ),
                              Padding(padding: EdgeInsets.only(top: 10.0)),
                              Text(
                                "Pays d'origine",
                                style: _subHeaderCustomStyle,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(marchandise.paysOrigine,
                                      style: _customTextStyle)
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 10.0)),
                              Text(
                                "Année de mise en circulation",
                                style: _subHeaderCustomStyle,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(marchandise.anneeMiseCirculation,
                                      style: _customTextStyle)
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    /// Background white for description
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration:
                        BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                            color: Color(0xFF6565).withOpacity(0.15),
                            blurRadius: 1.0,
                            spreadRadius: 0.2,
                          )
                        ]),
                        child: Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20.0),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .tr('description'),
                                  style: _subHeaderCustomStyle,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 15.0,
                                    right: 20.0,
                                    bottom: 10.0,
                                    left: 20.0),
                                child: Text(marchandise.description,
                                    style: _detailText),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    _suggestedItem
                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _buildRating(
      String date, String details, Function changeRating, String image) {
    return ListTile(
      leading: Container(
        height: 45.0,
        width: 45.0,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage(image), fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(50.0))),
      ),
      title: Row(
        children: <Widget>[
          StarRating(
              size: 20.0,
              rating: 3.5,
              starCount: 5,
              color: Colors.yellow,
              onRatingChanged: changeRating),
          SizedBox(width: 8.0),
          Text(
            date,
            style: TextStyle(fontSize: 12.0),
          )
        ],
      ),
      subtitle: Text(
        details,
        style: _detailText,
      ),
    );
  }
}

/// RadioButton for item choose in size
class RadioButtonCustom extends StatefulWidget {
  String txt;

  RadioButtonCustom({this.txt});

  @override
  _RadioButtonCustomState createState() => _RadioButtonCustomState(this.txt);
}

class _RadioButtonCustomState extends State<RadioButtonCustom> {
  _RadioButtonCustomState(this.txt);

  String txt;
  bool itemSelected = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          setState(() {
            if (itemSelected == false) {
              setState(() {
                itemSelected = true;
              });
            } else if (itemSelected == true) {
              setState(() {
                itemSelected = false;
              });
            }
          });
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
              shape: BoxShape.circle),
          child: Center(
            child: Text(
              txt,
              style: TextStyle(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
            ),
          ),
        ),
      ),
    );
  }
}

/// RadioButton for item choose in color
class RadioButtonColor extends StatefulWidget {
  Color clr;

  RadioButtonColor(this.clr);

  @override
  _RadioButtonColorState createState() => _RadioButtonColorState(this.clr);
}

class _RadioButtonColorState extends State<RadioButtonColor> {
  bool itemSelected = true;
  Color clr;

  _RadioButtonColorState(this.clr);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          if (itemSelected == false) {
            setState(() {
              itemSelected = true;
            });
          } else if (itemSelected == true) {
            setState(() {
              itemSelected = false;
            });
          }
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: clr,
              border: Border.all(
                  color: itemSelected ? Colors.black26 : Colors.indigoAccent,
                  width: 2.0),
              shape: BoxShape.circle),
        ),
      ),
    );
  }
}

/// Class for card product in "Top Rated Products"
class FavoriteItem extends StatelessWidget {
  String image, modele, Salary, title, anneMiseCirculation, paysOrigine;

  FavoriteItem(
      {this.image,
        this.modele,
        this.Salary,
        this.title,
        this.anneMiseCirculation,
        this.paysOrigine});

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Padding(
      padding: const EdgeInsets.only(left: 5.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF656565).withOpacity(0.15),
                blurRadius: 4.0,
                spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
              )
            ]),
        child: Wrap(children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 150.0,
                  width: 150.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(7.0),
                          topRight: Radius.circular(7.0)),
                      image: DecorationImage(
                          image: AssetImage(image), fit: BoxFit.fill)),
                ),
                Padding(padding: EdgeInsets.only(top: 15.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    title,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 1.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    Salary,
                    style: TextStyle(
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 2.0),
                  child: Text(
                    modele,
                    style: TextStyle(
                        fontFamily: "Sans",
                        color: Colors.black26,
                        fontWeight: FontWeight.w500,
                        fontSize: 12.0),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 2.0),
                  child: Text(
                    anneMiseCirculation,
                    style: TextStyle(
                        fontFamily: "Sans",
                        color: Colors.black26,
                        fontWeight: FontWeight.w500,
                        fontSize: 12.0),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 2.0),
                  child: Text(
                    paysOrigine,
                    style: TextStyle(
                        fontFamily: "Sans",
                        color: Colors.black26,
                        fontWeight: FontWeight.w500,
                        fontSize: 12.0),
                  ),
                )
              ]),
        ]),
      ),
    );
  }
}

Widget _line() {
  return Container(
    height: 0.9,
    width: double.infinity,
    color: Colors.black12,
  );
}
