
import 'dart:async';

import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_delegate.dart';
import 'package:eraus_group_app/Library/Language_Library/lib/easy_localization_provider.dart';
import 'package:eraus_group_app/Library/main_colors.dart';
import 'package:eraus_group_app/UI/BottomNavigationBar.dart';
import 'package:flutter/material.dart';
import 'package:eraus_group_app/ListItem/CartItemData.dart';
import 'package:eraus_group_app/UI/CartUIComponent/Delivery.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ConfirmationCommande extends StatefulWidget {
  @override
  _ConfirmationCommandeState createState() => _ConfirmationCommandeState();
}

class _ConfirmationCommandeState extends State<ConfirmationCommande> {

  final  List<CartItem> items = new List();

  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );


  @override
  void initState() {
    super.initState();
    setState(() {
      items.add(
        CartItem(
          img:"assets/img/bmw-918408_1920.jpg",
          id: 1,
          title: "BMW",
          desc:  "BWS Rapide et solide",
          price: "\$ 950",
        ),
      );
    });
  }
  /// Declare price and value for chart
  int value = 1;
  int pay = 950;

  /// Duration for popup card if user succes to payment
  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }

  /// Navigation to route after user succes payment
  void navigator() {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) => new BottomNavigationBarCustomized()));
  }

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Color(0xFF6991C7)),
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Confirmer l\'achat',
              style: TextStyle(
                  fontFamily: "Gotik",
                  fontSize: 18.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w700),
            ),
            elevation: 0.0,
          ),

          ///
          ///
          /// Checking item value of cart
          ///
          ///
          body: items.length>0?
          ListView.builder(
            itemCount: items.length,
            itemBuilder: (context,position){
              ///
              /// Widget for list view slide delete
              ///
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                secondaryActions: <Widget>[
                  new IconSlideAction(
                    key: Key(items[position].id.toString()),
                    caption: 'Supprimer',
                    color: Colors.red,
                    icon: Icons.delete,
                    onTap: () {
                      setState(() {
                        items.removeAt(position);
                      });
                      ///
                      /// SnackBar show if cart delet
                      ///
                      Scaffold.of(context)
                          .showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).tr('cartDeleted')),duration: Duration(seconds: 2),backgroundColor: Colors.redAccent,));
                    },
                  ),
                ],
                child: Padding(
                  padding: const EdgeInsets.only(top: 1.0, left: 13.0, right: 13.0),
                  /// Background Constructor for card
                  child: Container(
                    height: 220.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12.withOpacity(0.1),
                          blurRadius: 3.5,
                          spreadRadius: 0.4,
                        )
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(10.0),

                                /// Image item
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.1),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12.withOpacity(0.1),
                                              blurRadius: 0.5,
                                              spreadRadius: 0.1)
                                        ]),
                                    child: Image.asset('${items[position].img}',
                                      height: 130.0,
                                      width: 120.0,
                                      fit: BoxFit.fill,
                                    ))),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 25.0, left: 10.0, right: 5.0),
                                child: Column(

                                  /// Text Information Item
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${items[position].title}',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontFamily: "Sans",
                                        color: Colors.black87,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 10.0)),
                                    Text(
                                      '${items[position].desc}',
                                      style: TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 10.0)),
                                    Text('${items[position].price}'),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 18.0, left: 0.0),
                                      child: Container(
                                        width: 112.0,
                                        decoration: BoxDecoration(
                                            color: Colors.white70,
                                            border: Border.all(
                                                color: Colors.black12.withOpacity(0.1))),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceAround,
                                          children: <Widget>[

                                            /// Decrease of value item
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  value = value - 1;
                                                  pay= 950 * value;
                                                });
                                              },
                                              child: Container(
                                                height: 30.0,
                                                width: 30.0,
                                                decoration: BoxDecoration(
                                                    border: Border(
                                                        right: BorderSide(
                                                            color: Colors.black12
                                                                .withOpacity(0.1)))),
                                                child: Center(child: Text("-")),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.symmetric(
                                                  horizontal: 18.0),
                                              child: Text(value.toString()),
                                            ),

                                            /// Increasing value of item
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  value = value + 1;
                                                  pay = 950 * value;
                                                });
                                              },
                                              child: Container(
                                                height: 30.0,
                                                width: 28.0,
                                                decoration: BoxDecoration(
                                                    border: Border(
                                                        left: BorderSide(
                                                            color: Colors.black12
                                                                .withOpacity(0.1)))),
                                                child: Center(child: Text("+")),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.0)),
                        Divider(
                          height: 2.0,
                          color: Colors.black12,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 9.0, left: 10.0, right: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),

                                /// Total price of item buy
                                child: Text(
                                  AppLocalizations.of(context).tr('cartTotal')+"\$" + pay.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15.5,
                                      fontFamily: "Sans"),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  _showDialog(context);
                                  startTime();
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Container(
                                    height: 40.0,
                                    width: 120.0,
                                    decoration: BoxDecoration(
                                      color: MainColor.third,
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Confirmer',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "Sans",
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            scrollDirection: Axis.vertical,
          ): noItemCart()
      ),
    );
  }

  _showDialog(BuildContext ctx) {
    showDialog(
      context: ctx,
      barrierDismissible: true,
      child: SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  AppLocalizations.of(ctx).tr('yuppy'),
                  style: _txtCustomHead,
                ),
              )),
          Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
                child: Text(
                  AppLocalizations.of(ctx).tr('paymentReceive'),
                  style: _txtCustomSub,
                ),
              )),
        ],
      ),
    );
  }
}

///
///
/// If no item cart this class showing
///
class noItemCart extends StatelessWidget {

  /// Custom Text Header for Dialog after user succes payment

  @override

  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return  Container(
      width: 500.0,
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                EdgeInsets.only(top: mediaQueryData.padding.top + 50.0)),
            Image.asset(
              "assets/imgIllustration/IlustrasiCart.png",
              height: 300.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 10.0)),
            Text(
              AppLocalizations.of(context).tr('cartNoItem'),
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 18.5,
                  color: Colors.black26.withOpacity(0.2),
                  fontFamily: "Popins"),
            ),
          ],
        ),
      ),
    );
  }

}
